### Interface utilisateur responsive pour le site [http://www.jeuxdemots.org/](http://www.jeuxdemots.org/)

### Description

Le but de ce projet est ...

### Technologies utilisées

#### Pour la partie serveur

* Node.js
* MongoDB

#### Pour la partie client

...

### Installation du projet

##### Cloner le dépôt

    $ cd <répertoire d'installation>
    $ git clone git@gitlab.com:cmamon/jdm-interface.git

##### Installer Node.js

    #installeNodejssitulapasencorefait

##### Installer tous les modules nécessaires

Les modules sont téléchargés directement à partir du contenu des fichiers `package.json` (un pour le client et un pour le serveur).

Il suffit de lancer :

    $ npm install

### Informations pour le développement

#### Angular

###### Installer Angular CLI (Command Line Interface)

    $ npm install -g @angular/cli

###### Lancer l'application Angular

    $ ng serve

#### Exécution

En étant dans le dossier `server` :

<!-- ###### 1. Lancer MongoDB dans un terminal

    mongod --dbpath data --logpath /dev/null -->

###### Lancer le serveur Node.js

    $ node index.js

### Auteurs

* [Christophe QUENETTE](https://gitlab.info-ufr.univ-montp2.fr/u/e20140034671) - <christophe.quenette@etu.umontpellier.fr>
* [Imrhan Dareine MINKO AMOA](https://gitlab.info-ufr.univ-montp2.fr/u/e20150007990) - <imrhan-dareine.minko-amoa@etu.umontpellier.fr>
