import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { TermDetailComponent } from './components/term-detail/term-detail.component';
import { SearchHistoryComponent } from './components/search-history/search-history.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    {
        path: 'search-history',
        component: SearchHistoryComponent,
        runGuardsAndResolvers: 'always'
    },
    {
        path: 'term-detail/:term',
        component: TermDetailComponent,
        runGuardsAndResolvers: 'always'
    },
    { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes,  { onSameUrlNavigation : 'reload'})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
