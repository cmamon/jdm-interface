import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    storageAgreement: boolean;

    constructor(private router: Router) { }

    ngOnInit() {
        this.storageAgreement = localStorage.getItem('storageAgreement') === 'true';
    }

    searchHistory(): void {
        this.router.navigate(['/search-history']);
    }
}
