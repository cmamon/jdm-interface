import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    selector: 'app-search-history',
    templateUrl: './search-history.component.html',
    styleUrls: ['./search-history.component.css']
})
export class SearchHistoryComponent implements OnInit {
    searchHistory: any;
    navigationSubscription: any;

    constructor(private location: Location, private router: Router) {
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                this.ngOnInit();
            }
        });
    }

    ngOnInit() {
        this.searchHistory = JSON.parse(localStorage.getItem("search_history"));
    }

    clearHistory(): void {
        localStorage.setItem("search_history", JSON.stringify({}));
        this.router.navigate(['/search-history']);
    }

    goBack() {
        this.location.back();
    }

    goToHome(): void {
        this.router.navigate(['/']);
    }
}
