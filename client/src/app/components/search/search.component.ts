import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TermService } from '../../services/term.service';
import { serverUrl } from '../../config';
import { fromEvent, of } from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import relationshipsNames from '../../../assets/json/relationshipsNames.json';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    termControl = new FormControl('', Validators.required);
    isSpinnerVisible = false;
    reloadCurrentRoute = false;
    suggestions: any;
    relationshipsNames = relationshipsNames;

    constructor(
        private termService: TermService,
        private router: Router,
        private modalService: NgbModal
    ) { }

    ngOnInit(): void {
        const termInput = document.getElementById('term-input');

        const typeahead = fromEvent(termInput, 'input').pipe(
            map((e: KeyboardEvent) => (<HTMLInputElement>e.target).value),
            filter(text => text.length > 2),
            debounceTime(10),
            distinctUntilChanged(),
            switchMap((text) => ajax({
                url: serverUrl + 'typeahead',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'rxjs-custom-header': 'Rxjs'
                },
                body: {
                    input: text
                }
            }))
        );

        typeahead.subscribe(
            data => {
                this.suggestions = data.response;
            },
            error => {
                return of(error);
            }
        );
    }

    onSubmit(): void {
        this.isSpinnerVisible = true;

        // Remove html tags
        const input = this.termControl.value.replace(/<[^>]*>?/gm, '');

        const typesButton = (<HTMLInputElement>document.getElementById('rel-types-button'));

        let rel = ''
        if (typesButton.name && typesButton.name != '-1') {
            rel = typesButton.name;
        }

        this.termService.retrieveDump(input, rel)
            .pipe(first())
            .subscribe(
                 data => {
                     this.isSpinnerVisible = false;
                     this.termControl.setValue(input);
                     this.termService.setStartTime(performance.now());
                     this.termService.setTerm(input);
                     this.termService.setTermData(data);
                     this.addToHistory(input); // Add searched term to search history
                     this.router.navigate(['/term-detail',input]);
                 },
                 error => {
                     this.isSpinnerVisible = false;
                     console.error(error);
                 }
            );
    }

    addToHistory(input: string): void {
        // Get time of search
        const searchDateTime = new Date();
        const searchDate = searchDateTime.toLocaleDateString()
        const searchTime = searchDateTime.toLocaleTimeString(
            'en-GB',
            { hour: "numeric", minute: "numeric" }
        );

        let history = JSON.parse(localStorage.getItem("search_history"));

        if (!history) {
            history = {};
        }

        if (history[searchDate]) {
            history[searchDate].unshift({ term: input, time: searchTime});
        } else {
            history = Object.assign({
                [searchDate]: [{ term: input, time: searchTime}]
            }, history);
        }

        localStorage.setItem("search_history", JSON.stringify(history));
    }

    open(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });

        let relTypesButton = <HTMLInputElement>document.getElementById('rel-types-button');
        relTypesButton.innerHTML = '<i class="fa fa-plus-circle"></i>';
        relTypesButton.name = '-1';
    }

    setRelType(value) {
        (<HTMLInputElement>document.getElementById('relationship-types')).value = value;

        let relTypesButton = <HTMLInputElement>document.getElementById('rel-types-button');
        relTypesButton.name = value.toString();

        if (value == -1) {
            relTypesButton.innerHTML = '<i class="fa fa-plus-circle"></i>';
            return;
        }

        relTypesButton.innerHTML = relationshipsNames[value].name;
    }
}
