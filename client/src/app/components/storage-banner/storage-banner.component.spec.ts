import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageBannerComponent } from './storage-banner.component';

describe('StorageBannerComponent', () => {
    let component: StorageBannerComponent;
    let fixture: ComponentFixture<StorageBannerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ StorageBannerComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StorageBannerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
