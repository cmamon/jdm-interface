import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage-banner',
  templateUrl: './storage-banner.component.html',
  styleUrls: ['./storage-banner.component.css']
})
export class StorageBannerComponent implements OnInit {

    constructor() { }

    ngOnInit() {
        localStorage.setItem('storageAgreement', 'true');
    }

    removeBanner() {
        const storageBanner = document.getElementById('storage-banner');
        storageBanner.style.display = 'none';
    }

    refuseStorage() {
        // L'utilisateur refuse le stockage d'informations
        localStorage.setItem('storageAgreement', 'false');

        // On lui empèche l'accès aux autres pages du sites et on lui affiche
        // qqch lui disant que s'il veut avoir l'historique de ses recherches il
        // doit accepter le stockage.
    }

}
