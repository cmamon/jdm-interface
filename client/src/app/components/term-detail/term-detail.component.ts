import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TermService } from '../../services/term.service';
import { SearchComponent } from '../search/search.component';
import relationshipsNames from '../../../assets/json/relationshipsNames.json';
import { first } from 'rxjs/operators';

@Component({
    selector: 'app-term-detail',
    templateUrl: './term-detail.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./term-detail.component.css']
})

export class TermDetailComponent implements OnInit, OnDestroy {
    @ViewChild(SearchComponent) searchComponent: SearchComponent ;

    private termDump: string;

    associatedTerms: any = [];
    incomingTerms: any = [];
    definitions: any = [];
    navigationSubscription: any;
    nbDefinitions: number;
    nbRefinements = 0;
    nbTerms: number;
    nbIncTerms: number;
    pageIncRels = 1;
    pageIncRelsSize = 25;
    pageOutRels = 1;
    pageOutRelsSize = 25;
    semanticRefinements: any = [];
    term: string;
    termExists: boolean;
    outgoingRelationships = {};
    incomingRelationships = {};
    termsRelationId: any = [];
    incomingRelationId: any = [];
    availableRelationships: any = [];
    relationshipsNames = relationshipsNames;

    constructor(private termService: TermService, private router: Router) {
        this.navigationSubscription = this.router.events.subscribe((e: any) => {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof NavigationEnd) {
                this.ngOnInit();
            }
        });
    }

    ngOnInit() {
        this.term = this.termService.getTerm();
        this.termDump = this.termService.getTermDump();
        this.outgoingRelationships = this.termService.getTermOutRels();
        this.incomingRelationships = this.termService.getTermInRels();

        if (!this.checkTermValidity()) {
            return;
        }

        this.semanticRefinements = this.termService.getTermData()
            .semanticRefinements;
        this.searchComponent.termControl.setValue(this.term);
        this.searchComponent.reloadCurrentRoute = true;
        this.displayDump();
        console.log(
            'Temps total de la requête : '
            + this.termService.getRequestDuration() + ' ms.'
        );
    }

    ngOnDestroy() {
       // avoid memory leaks here by cleaning up after ourselves. If we
       // don't then we will continue to run our initialiseInvites()
       // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    }

    checkTermValidity(): boolean {
        // Si le terme n'est pas stocké retourner à la page d'accueil
        if (this.term == null) {
            this.router.navigate(['/']);
            return false;
        }

        // Si le serveur indique que le mot n'existe pas, le signaler
        if (this.termDump.includes('\'' + this.term + '\' n\'existe pas !')) {
            this.termExists = false;
            return false;
        }
        this.termExists = true;

        return true;
    }

    displayDump(): void {
        // Récupération des définitions
        let definitions = this.termDump.split('<def>').pop().split('</def>')[0];
        // Suppression des sauts de lignes inutiles
        definitions = definitions.replace(/\\n/g, '');

        // On récupère le nombre de refinements sémantiques
        this.nbRefinements = Object.keys(this.semanticRefinements).length;

        this.displayDefinitions(definitions);
        this.displaySemanticRefinements(this.semanticRefinements);
        this.displayTerms(this.outgoingRelationships);
        this.displayIncomingTerms(this.incomingRelationships);
    }

    displayDefinitions(dump: string): void {
        // Récupérer les définitions une par une
        this.definitions = dump.split(/<br \/>\d\./);
        this.definitions.shift();

        // Supprimer les balises <br />
        this.definitions.forEach((def, index, arr) => {
            arr[index] = def.replace(/<br \/>/g, '\n');
        });

        this.nbDefinitions = this.definitions.length;
    }

    displaySemanticRefinements(refinements): void {
        this.semanticRefinements = [];

        for (const refinement in refinements) {
            if (refinements.hasOwnProperty(refinement)) {
                this.semanticRefinements.push({
                    'value' : refinement,
                    'defs' : refinements[refinement].replace(/\\n/g, '\n')
                });
            }
        }
    }

    displayTerms(outgoingRelationships: Object): void {
        this.availableRelationships = [];
        for (const key in outgoingRelationships) {
            if (outgoingRelationships.hasOwnProperty(key)) {
                this.availableRelationships.push(key);
            }
        }

        this.showRelation(this.availableRelationships[Object.keys(this.availableRelationships)[0]]);
    }

    displayIncomingTerms(incomingRelationships: Object): void {
        for (const key in incomingRelationships) {
            if (incomingRelationships.hasOwnProperty(key)) {
                if(!this.availableRelationships.includes(key)){
                    this.availableRelationships.push(key);
                }
            }
        }
    }

    showRelation(relationId: number, event = null) {

        if (this.outgoingRelationships.hasOwnProperty(relationId)) {
            this.termsRelationId = this.outgoingRelationships[relationId];
        } else{
            this.termsRelationId = [];
        }

        if(this.incomingRelationships.hasOwnProperty(relationId)){
            this.incomingRelationId = this.incomingRelationships[relationId];
        } else {
            this.incomingRelationId = [];
        }

        this.sortByWeight();
        this.sortByWeightIncomingTerms();
        this.associatedTerms = [];
        this.incomingTerms = [];
        this.nbTerms = this.termsRelationId.length;
        this.nbIncTerms = this.incomingRelationId.length;

        for (const termData of this.termsRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.associatedTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId});
        }

        for (const termData of this.incomingRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.incomingTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId });
        }

    }

    /* *
     * Tri des termes par ordre alphabétique
     */
    sortAlphabetically(): void {
        this.associatedTerms = [];

        const collator = new Intl.Collator('fr', {
            numeric: true,
            sensitivity: 'base'
        });

        this.termsRelationId.sort((a, b) => collator.compare(a.term, b.term));

        for (const termData of this.termsRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.associatedTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId });
        }
    }

    /* *
     * Tri des termes par poids de relation
     */
    sortByWeight(): void {
        this.associatedTerms = [];

        this.nbTerms = this.termsRelationId.length;

        // Tri des relations par poids
        this.termsRelationId.sort((a, b) => b.weight - a.weight);

        for (const termData of this.termsRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.associatedTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId });
        }
    }

    /* *
     * Tri des relations sortantes par ordre alphabétique
     */
    sortAlphabeticallyIncomingTerms(): void {
        this.incomingTerms = [];

        const collator = new Intl.Collator('fr', {
            numeric: true,
            sensitivity: 'base'
        });

        this.incomingRelationId.sort((a, b) => collator.compare(a.term, b.term));

        for (const termData of this.incomingRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.incomingTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId});
        }
    }

    /* *
     * Tri des relations sortantes par poids de relation
     */
    sortByWeightIncomingTerms(): void {
        this.incomingTerms = [];
        // Tri des relations par poids
        this.incomingRelationId.sort((a, b) => b.weight - a.weight);

        this.nbIncTerms = this.incomingRelationId.length;

        for (const termData of this.incomingRelationId) {
            const value = termData.term;
            const weight = termData.weight;
            const relationId = termData.relationId;
            this.incomingTerms.push({ 'value' : value, 'weight' : weight, 'relationId' : relationId });
        }
    }

    onResearch(input: string): void {

        this.termService.retrieveDump(input)
            .pipe(first())
            .subscribe(
                 data => {
                     this.termService.setStartTime(performance.now());
                     this.termService.setTerm(input);
                     this.termService.setTermData(data);
                     this.addToHistory(input); // Add searched term to search history
                     this.router.navigate(['/term-detail',input]);
                     window.scroll(0,0);
                 },
                 error => {
                     console.error(error);
                 }
            );
    }

    addToHistory(input: string): void {
        // Get time of search
        const searchDateTime = new Date();
        const searchDate = searchDateTime.toLocaleDateString()
        const searchTime = searchDateTime.toLocaleTimeString(
            'en-GB',
            { hour: "numeric", minute: "numeric" }
        );

        let history = JSON.parse(localStorage.getItem("search_history"));

        if (!history) {
            history = {};
        }

        if (history[searchDate]) {
            history[searchDate].unshift({ term: input, time: searchTime});
        } else {
            history = Object.assign({
                [searchDate]: [{ term: input, time: searchTime}]
            }, history);
        }

        localStorage.setItem("search_history", JSON.stringify(history));
    }
}
