import { isDevMode } from '@angular/core';

let origin = window.location.origin;

if (origin === 'http://localhost:4200') {
    origin = 'http://localhost:8888';
}

export const serverUrl = origin + '/api/';
