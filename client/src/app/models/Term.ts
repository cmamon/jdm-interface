export interface Term {
    dump: string;
    semanticRefinements: object;
}
