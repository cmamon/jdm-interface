import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Term } from '../models/Term';
import { serverUrl } from '../config';

@Injectable({ providedIn: 'root' })

export class TermService {
    private term: string;
    private rel: string;
    private data;
    private startTime: number;

    constructor(private http: HttpClient) { }

    retrieveDump(term: string, rel: string = '') {
        const httpOptions = {
            headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
        };

        return this.http.post<Term>(serverUrl, {
            'term' : term,
            'rel' : rel
        }, httpOptions);
    }

    getTerm() {
        return this.term;
    }

    setTerm(term) {
        this.term = term;
    }

    getRel() {
        return this.rel;
    }

    setRel(rel) {
        if (rel) {
            this.rel = rel;
        }
    }

    getTermDump() {
        return this.data != null ? this.data.dump : null;
    }

    getTermOutRels() {
        return this.data != null ? this.data.outgoingRelationships : null;
    }

    getTermInRels() {
        return this.data != null ? this.data.incomingRelationships : null;
    }

    getTermData() {
        return this.data;
    }

    setTermData(data) {
        this.data = data;
    }

    getRequestDuration() {
        return performance.now() - this.startTime;
    }

    setStartTime(time: number) {
        this.startTime = time;
    }
}
