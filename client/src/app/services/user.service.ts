import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { serverUrl } from '../config';

import { User } from '../models/User';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(serverUrl + 'users');
    }

    getById(email: string) {
        return this.http.get(serverUrl + 'users/' + email);
    }

    register(user: User) {
        return this.http.post(serverUrl + 'users/signup', user);
    }

    update(user: User) {
        return this.http.put(serverUrl + 'users/' + user.email, user);
    }

    delete(email: string) {
        return this.http.delete(serverUrl + 'users/' + email);
    }
}
