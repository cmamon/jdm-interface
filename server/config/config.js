const JDM_URL = 'http://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel=';
const NodeCache = require('node-cache');
const rateLimit = require('express-rate-limit');

// Time To Live pour un terme : un mois, fréquence de vérification : chaque jour
const cache = new NodeCache({
    stdTTL : 2592000,
    checkperiod : 86400,
    useClones : false
});

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100 // limit each IP to 100 requests per windowMs
});


module.exports = {cache, limiter, JDM_URL};
