const express = require('express');
const router = express.Router();
const termController = require('../src/controllers/term_controller.js');
const userController = require('../src/controllers/user_controller.js');

// Terms routes
router.post('/', termController.getDump);
router.post('/typeahead', termController.typeahead);

// Users routes
router.get('/', userController.list);
router.get('/:email', userController.getUserByEmail);

router.post('/signup', userController.register);
router.post('/login', userController.login);

router.delete('/:email', userController.deleteUser);

module.exports = router;
