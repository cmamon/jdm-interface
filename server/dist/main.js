(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_term_detail_term_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/term-detail/term-detail.component */ "./src/app/components/term-detail/term-detail.component.ts");
/* harmony import */ var _components_search_history_search_history_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/search-history/search-history.component */ "./src/app/components/search-history/search-history.component.ts");






var routes = [
    { path: '', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    {
        path: 'search-history',
        component: _components_search_history_search_history_component__WEBPACK_IMPORTED_MODULE_5__["SearchHistoryComponent"],
        runGuardsAndResolvers: 'always'
    },
    {
        path: 'term-detail/:term',
        component: _components_term_detail_term_detail_component__WEBPACK_IMPORTED_MODULE_4__["TermDetailComponent"],
        runGuardsAndResolvers: 'always'
    },
    { path: '**', redirectTo: '' }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { onSameUrlNavigation: 'reload' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\" class=\"page-content\">\n    <header>\n        <app-header></app-header>\n    </header>\n    <router-outlet class=\"content\"></router-outlet>\n</div>\n<footer class=\"footer text-white bg-gradient-dark py-4\">\n    <app-footer></app-footer>\n</footer>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'client';
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_search_search_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/search/search.component */ "./src/app/components/search/search.component.ts");
/* harmony import */ var _components_term_detail_term_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/term-detail/term-detail.component */ "./src/app/components/term-detail/term-detail.component.ts");
/* harmony import */ var _components_storage_banner_storage_banner_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/storage-banner/storage-banner.component */ "./src/app/components/storage-banner/storage-banner.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_search_history_search_history_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/search-history/search-history.component */ "./src/app/components/search-history/search-history.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/components/footer/footer.component.ts");















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
                _components_search_search_component__WEBPACK_IMPORTED_MODULE_9__["SearchComponent"],
                _components_storage_banner_storage_banner_component__WEBPACK_IMPORTED_MODULE_11__["StorageBannerComponent"],
                _components_term_detail_term_detail_component__WEBPACK_IMPORTED_MODULE_10__["TermDetailComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_12__["HeaderComponent"],
                _components_search_history_search_history_component__WEBPACK_IMPORTED_MODULE_13__["SearchHistoryComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_14__["FooterComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_4__["NgbModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/footer/footer.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container text-center text-white-50\">\n    <p class=\"text-xs-center\"> JDM Interface &copy; 2020 </p>\n</div>\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/components/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".heading {\n    @include text-hide;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7QUFDdEIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkaW5nIHtcbiAgICBAaW5jbHVkZSB0ZXh0LWhpZGU7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-storage-banner *ngIf=\"!storageAgreement\"></app-storage-banner>\n\n<nav class=\"navbar navbar-light bg-light\">\n    <a class=\"navbar-brand\" href=\"#\">\n        <img src=\"../../../assets/images/JDM.png\"\n             width=\"40\"\n             height=\"40\"\n             class=\"d-inline-block align-top rounded\"\n             alt=\"Jeux de mots\"\n        >\n        Le Diko\n    </a>\n    <form class=\"form-inline my-lg-0\">\n        <button class=\"btn btn-sm btn-outline-dark rounded rounded-pill grey-hover\" type=\"button\"\n            (click)=\"searchHistory()\"\n        >\n            Historique\n        </button>\n    </form>\n</nav>\n<br>\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.storageAgreement = localStorage.getItem('storageAgreement') === 'true';
    };
    HeaderComponent.prototype.searchHistory = function () {
        this.router.navigate(['/search-history']);
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h4 {\n    text-align: center;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoNCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h4>Bienvenue sur l'interface de recherche avancée de\n        <a class=\"text-decoration-none\" href=\"http://www.jeuxdemots.org\">\n            Jeux de mots\n        </a>\n    </h4>\n    <br>\n    <app-search></app-search>\n    <section class=\"container col-lg-6 col-sm-8\">\n        <div class=\"mx-auto\">\n            <p class=\"text-center\">\n                Rechechez un terme de la langue française pour obtenir des\n                informations le concernant\n            </p>\n        </div>\n    </section>\n</div>\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/search-history/search-history.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/search-history/search-history.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2VhcmNoLWhpc3Rvcnkvc2VhcmNoLWhpc3RvcnkuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/search-history/search-history.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/search-history/search-history.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-sm-2 col-md-1 col-lg-2\">\n\n        </div>\n        <div class=\"col-sm-8 col-md-10 col-lg-8\">\n            <div *ngIf=\"(searchHistory | json) != '{}'\">\n                <div class=\"container\">\n                    <button type=\"button\" name=\"button\" class=\"btn btn-block btn-sm grey-hover btn-secondary rounded rounded-pill\"\n                        (click)=\"goBack()\">\n                        Retour à la recherche\n                    </button>\n                    <button type=\"button\" name=\"button\" class=\"btn btn-block btn-sm btn-danger grey-hover rounded rounded-pill\" (click)=\"clearHistory()\">\n                        Supprimer l'historique\n                    </button>\n                </div>\n                <br>\n                <div *ngFor=\"let date of searchHistory | keyvalue\" class=\"container\">\n                    <ul class=\"list-group list-definitions\">\n                        <li class=\"list-group-item d-flex justify-content-between align-items-center active list-group-item-primary\">\n                            Le {{ date.key }}\n                        </li>\n                        <li *ngFor=\"let search of date.value\"\n                        class=\"list-group-item\">\n                            <small><b>{{ search.term }}</b> à {{ search.time }}</small>\n                        </li>\n                    </ul>\n                    <br>\n                </div>\n            </div>\n             <div *ngIf=\"(searchHistory | json) == '{}'\" class=\"container\">\n                 <p>Votre historique de recherche est vide.</p>\n                 <button type=\"button\" name=\"button\" class=\"btn grey-hover btn-secondary\"\n                 (click)=\"goToHome()\">\n                     Retour à la recherche\n                 </button>\n             </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/search-history/search-history.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/search-history/search-history.component.ts ***!
  \***********************************************************************/
/*! exports provided: SearchHistoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchHistoryComponent", function() { return SearchHistoryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");




var SearchHistoryComponent = /** @class */ (function () {
    function SearchHistoryComponent(location, router) {
        var _this = this;
        this.location = location;
        this.router = router;
        this.navigationSubscription = this.router.events.subscribe(function (e) {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.ngOnInit();
            }
        });
    }
    SearchHistoryComponent.prototype.ngOnInit = function () {
        this.searchHistory = JSON.parse(localStorage.getItem("search_history"));
    };
    SearchHistoryComponent.prototype.clearHistory = function () {
        localStorage.setItem("search_history", JSON.stringify({}));
        this.router.navigate(['/search-history']);
    };
    SearchHistoryComponent.prototype.goBack = function () {
        this.location.back();
    };
    SearchHistoryComponent.prototype.goToHome = function () {
        this.router.navigate(['/']);
    };
    SearchHistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search-history',
            template: __webpack_require__(/*! ./search-history.component.html */ "./src/app/components/search-history/search-history.component.html"),
            styles: [__webpack_require__(/*! ./search-history.component.css */ "./src/app/components/search-history/search-history.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common__WEBPACK_IMPORTED_MODULE_3__["Location"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], SearchHistoryComponent);
    return SearchHistoryComponent;
}());



/***/ }),

/***/ "./src/app/components/search/search.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/search/search.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".search-form {\n    text-align: center;\n    margin: auto;\n}\n\n.form-control:focus {\n  box-shadow: none;\n}\n\n.form-control-underlined {\n  border-width: 0;\n  border-bottom-width: 1px;\n  border-radius: 0;\n  padding-left: 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zZWFyY2gvc2VhcmNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLGVBQWU7RUFDZix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCLGVBQWU7QUFDakIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3NlYXJjaC9zZWFyY2guY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zZWFyY2gtZm9ybSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbjogYXV0bztcbn1cblxuLmZvcm0tY29udHJvbDpmb2N1cyB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5mb3JtLWNvbnRyb2wtdW5kZXJsaW5lZCB7XG4gIGJvcmRlci13aWR0aDogMDtcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogMXB4O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/search/search.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/search/search.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"search-form container col-lg-8 col-sm-8 mx-auto\">\n    <form (ngSubmit)=\"onSubmit()\">\n        <ng-template #content let-modal>\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title text-center\" id=\"modal-basic-title\">Sélectionnez un type de relation</h5>\n                <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"modal.dismiss('Cross click')\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"modal-body\">\n                <div class=\"form-group p-1 bg-light\">\n                    <label for=\"relationship-types\">Type de relation </label>\n                    <div class=\"input-group\">\n                        <select class=\"custom-select rounded rounded-pill bg-light shadow-sm\"\n                            (change)=\"setRelType($event.target.value)\" id=\"relationship-types\"\n                        >\n                            <option value=\"-1\">Choisissez</option>\n                            <option *ngFor=\"let rel of relationshipsNames | keyvalue\" value=\"{{rel.key}}\">{{rel.value.name}}</option>\n                        </select>\n                    </div>\n                </div>\n            </div>\n            <!-- <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-sm btn-outline-dark rounded rounded-pill grey-hover\"\n                    (click)=\"modal.close('Save click')\"\n                >\n                    Valider\n                </button>\n            </div> -->\n        </ng-template>\n        <div class=\"p-1 bg-light rounded rounded-pill shadow-sm mb-4\">\n            <div class=\"input-group\">\n                <div class=\"input-group-prepend\">\n                    <button id=\"rel-types-button\" type=\"button\" (click)=\"open(content)\" class=\"btn btn-link text-primary\">\n                        <i class=\"fa fa-plus-circle\"></i>\n                    </button>\n                </div>\n                <input type=\"search\"\n                    placeholder=\"Saisissez un terme\"\n                    aria-describedby=\"button-addon1\"\n                    id=\"term-input\"\n                    list=\"suggestionsIds\"\n                    [formControl]=\"termControl\"\n                    class=\"form-control border-0 bg-light\"\n                    required>\n                <datalist id=\"suggestionsIds\">\n                    <option *ngFor=\"let suggestion of suggestions\" >\n                        {{suggestion}}\n                    </option>\n                </datalist>\n                <div class=\"input-group-append\">\n                    <button id=\"button-addon1\" type=\"submit\" class=\"btn btn-link text-primary\">\n                        <span *ngIf=\"isSpinnerVisible\"\n                            class=\"spinner-grow spinner-grow-sm\"\n                            role=\"status\" aria-hidden=\"true\">\n                        </span><i class=\"fa fa-search\"></i>\n                    </button>\n                </div>\n            </div>\n        </div>\n    </form>\n</div>\n<br>\n"

/***/ }),

/***/ "./src/app/components/search/search.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/search/search.component.ts ***!
  \*******************************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_term_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/term.service */ "./src/app/services/term.service.ts");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../config */ "./src/app/config.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_ajax__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs/ajax */ "./node_modules/rxjs/_esm5/ajax/index.js");
/* harmony import */ var _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../assets/json/relationshipsNames.json */ "./src/assets/json/relationshipsNames.json");
var _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_10___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../assets/json/relationshipsNames.json */ "./src/assets/json/relationshipsNames.json", 1);












var SearchComponent = /** @class */ (function () {
    function SearchComponent(termService, router, modalService) {
        this.termService = termService;
        this.router = router;
        this.modalService = modalService;
        this.termControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required);
        this.isSpinnerVisible = false;
        this.reloadCurrentRoute = false;
        this.relationshipsNames = _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_10__;
    }
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        var termInput = document.getElementById('term-input');
        var typeahead = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["fromEvent"])(termInput, 'input').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (e) { return e.target.value; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (text) { return text.length > 2; }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["debounceTime"])(10), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (text) { return Object(rxjs_ajax__WEBPACK_IMPORTED_MODULE_9__["ajax"])({
            url: _config__WEBPACK_IMPORTED_MODULE_7__["serverUrl"] + 'typeahead',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'rxjs-custom-header': 'Rxjs'
            },
            body: {
                input: text
            }
        }); }));
        typeahead.subscribe(function (data) {
            _this.suggestions = data.response;
        }, function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["of"])(error);
        });
    };
    SearchComponent.prototype.onSubmit = function () {
        var _this = this;
        this.isSpinnerVisible = true;
        // Remove html tags
        var input = this.termControl.value.replace(/<[^>]*>?/gm, '');
        var typesButton = document.getElementById('rel-types-button');
        var rel = '';
        if (typesButton.name && typesButton.name != '-1') {
            rel = typesButton.name;
        }
        console.log(rel);
        this.termService.retrieveDump(input, rel)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(function (data) {
            _this.isSpinnerVisible = false;
            _this.termControl.setValue(input);
            _this.termService.setStartTime(performance.now());
            _this.termService.setTerm(input);
            _this.termService.setTermData(data);
            _this.addToHistory(input); // Add searched term to search history
            _this.router.navigate(['/term-detail', input]);
        }, function (error) {
            _this.isSpinnerVisible = false;
            console.error(error);
        });
    };
    SearchComponent.prototype.addToHistory = function (input) {
        var _a;
        // Get time of search
        var searchDateTime = new Date();
        var searchDate = searchDateTime.toLocaleDateString();
        var searchTime = searchDateTime.toLocaleTimeString('en-GB', { hour: "numeric", minute: "numeric" });
        var history = JSON.parse(localStorage.getItem("search_history"));
        if (!history) {
            history = {};
        }
        if (history[searchDate]) {
            history[searchDate].unshift({ term: input, time: searchTime });
        }
        else {
            history = Object.assign((_a = {},
                _a[searchDate] = [{ term: input, time: searchTime }],
                _a), history);
        }
        localStorage.setItem("search_history", JSON.stringify(history));
    };
    SearchComponent.prototype.open = function (content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
        var relTypesButton = document.getElementById('rel-types-button');
        relTypesButton.innerHTML = '<i class="fa fa-plus-circle"></i>';
        relTypesButton.name = '-1';
    };
    SearchComponent.prototype.setRelType = function (value) {
        document.getElementById('relationship-types').value = value;
        var relTypesButton = document.getElementById('rel-types-button');
        relTypesButton.name = value.toString();
        if (value == -1) {
            relTypesButton.innerHTML = '<i class="fa fa-plus-circle"></i>';
            return;
        }
        relTypesButton.innerHTML = _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_10__[value].name;
    };
    SearchComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(/*! ./search.component.html */ "./src/app/components/search/search.component.html"),
            styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/components/search/search.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_term_service__WEBPACK_IMPORTED_MODULE_6__["TermService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/components/storage-banner/storage-banner.component.css":
/*!************************************************************************!*\
  !*** ./src/app/components/storage-banner/storage-banner.component.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#storage-banner {\n    width: auto;\n    background: #7F7FFF;\n    margin-top: 0px;\n}\n\n#storage-banner p {\n    padding : 10px;\n    font-size : 1em;\n    font-weight : bold;\n    text-align : center;\n    color : white;\n}\n\n#close-storage-banner {\n    text-align : right;\n}\n\n#close-storage-banner:hover {\n    cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zdG9yYWdlLWJhbm5lci9zdG9yYWdlLWJhbm5lci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLG1CQUFtQjtJQUNuQixlQUFlO0FBQ25COztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGFBQWE7QUFDakI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxlQUFlO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zdG9yYWdlLWJhbm5lci9zdG9yYWdlLWJhbm5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3N0b3JhZ2UtYmFubmVyIHtcbiAgICB3aWR0aDogYXV0bztcbiAgICBiYWNrZ3JvdW5kOiAjN0Y3RkZGO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbn1cblxuI3N0b3JhZ2UtYmFubmVyIHAge1xuICAgIHBhZGRpbmcgOiAxMHB4O1xuICAgIGZvbnQtc2l6ZSA6IDFlbTtcbiAgICBmb250LXdlaWdodCA6IGJvbGQ7XG4gICAgdGV4dC1hbGlnbiA6IGNlbnRlcjtcbiAgICBjb2xvciA6IHdoaXRlO1xufVxuXG4jY2xvc2Utc3RvcmFnZS1iYW5uZXIge1xuICAgIHRleHQtYWxpZ24gOiByaWdodDtcbn1cblxuI2Nsb3NlLXN0b3JhZ2UtYmFubmVyOmhvdmVyIHtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/storage-banner/storage-banner.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/storage-banner/storage-banner.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"storage-banner\" class=\"fixed-top\">\n    <p>\n        Ce site web enregistre des informations lors de votre navigation pour\n        vous offrir une meilleure expérience.\n        En poursuivant votre navigation, vous acceptez leur utilisation.\n        <button type=\"button\" class=\"close\" aria-label=\"Close\">\n            <span id=\"close-storage-banner\" (click)=\"removeBanner();\">\n                &times;\n            </span>\n        </button>\n    </p>\n</div>\n"

/***/ }),

/***/ "./src/app/components/storage-banner/storage-banner.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/storage-banner/storage-banner.component.ts ***!
  \***********************************************************************/
/*! exports provided: StorageBannerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageBannerComponent", function() { return StorageBannerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StorageBannerComponent = /** @class */ (function () {
    function StorageBannerComponent() {
    }
    StorageBannerComponent.prototype.ngOnInit = function () {
        localStorage.setItem('storageAgreement', 'true');
    };
    StorageBannerComponent.prototype.removeBanner = function () {
        var storageBanner = document.getElementById('storage-banner');
        storageBanner.style.display = 'none';
    };
    StorageBannerComponent.prototype.refuseStorage = function () {
        // L'utilisateur refuse le stockage d'informations
        localStorage.setItem('storageAgreement', 'false');
        // On lui empèche l'accès aux autres pages du sites et on lui affiche
        // qqch lui disant que s'il veut avoir l'historique de ses recherches il
        // doit accepter le stockage.
    };
    StorageBannerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-storage-banner',
            template: __webpack_require__(/*! ./storage-banner.component.html */ "./src/app/components/storage-banner/storage-banner.component.html"),
            styles: [__webpack_require__(/*! ./storage-banner.component.css */ "./src/app/components/storage-banner/storage-banner.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StorageBannerComponent);
    return StorageBannerComponent;
}());



/***/ }),

/***/ "./src/app/components/term-detail/term-detail.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/term-detail/term-detail.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".relations-buttons {\n    width: 100%;\n}\n\nlabel > input[type=radio] {\n    display: none;\n}\n\n.btn-cool-blues {\n    background: #007bff;  /* fallback for old browsers */  /* Chrome 10-25, Safari 5.1-6 */\n    background: -webkit-gradient(linear, left top, right top, from(#0062E6), to(#33AEFF));\n    background: linear-gradient(to right, #0062E6, #33AEFF); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n    color: #fff;\n}\n\n.my-custom-class .tooltip-inner {\n    background: #007bff;  /* fallback for old browsers */  /* Chrome 10-25, Safari 5.1-6 */\n    background: -webkit-gradient(linear, left top, right top, from(#4D5358), to(#4B5156));\n    background: linear-gradient(to right, #4D5358, #4B5156); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n    font-size: 90%;\n}\n\n.my-custom-class .arrow::before {\n    border-top-color: #4D5358;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90ZXJtLWRldGFpbC90ZXJtLWRldGFpbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztBQUNmOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLG1CQUFtQixHQUFHLDhCQUE4QixHQUNjLCtCQUErQjtJQUNqRyxxRkFBdUQ7SUFBdkQsdURBQXVELEVBQUUscUVBQXFFO0lBQzlILFdBQVc7QUFDZjs7QUFFQTtJQUNJLG1CQUFtQixHQUFHLDhCQUE4QixHQUNjLCtCQUErQjtJQUNqRyxxRkFBdUQ7SUFBdkQsdURBQXVELEVBQUUscUVBQXFFO0lBQzlILGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSx5QkFBeUI7QUFDN0IiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Rlcm0tZGV0YWlsL3Rlcm0tZGV0YWlsLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucmVsYXRpb25zLWJ1dHRvbnMge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5sYWJlbCA+IGlucHV0W3R5cGU9cmFkaW9dIHtcbiAgICBkaXNwbGF5OiBub25lO1xufVxuXG4uYnRuLWNvb2wtYmx1ZXMge1xuICAgIGJhY2tncm91bmQ6ICMwMDdiZmY7ICAvKiBmYWxsYmFjayBmb3Igb2xkIGJyb3dzZXJzICovXG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMwMDYyRTYsICMzM0FFRkYpOyAgLyogQ2hyb21lIDEwLTI1LCBTYWZhcmkgNS4xLTYgKi9cbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICMwMDYyRTYsICMzM0FFRkYpOyAvKiBXM0MsIElFIDEwKy8gRWRnZSwgRmlyZWZveCAxNissIENocm9tZSAyNissIE9wZXJhIDEyKywgU2FmYXJpIDcrICovXG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5teS1jdXN0b20tY2xhc3MgLnRvb2x0aXAtaW5uZXIge1xuICAgIGJhY2tncm91bmQ6ICMwMDdiZmY7ICAvKiBmYWxsYmFjayBmb3Igb2xkIGJyb3dzZXJzICovXG4gICAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM0RDUzNTgsICM0QjUxNTYpOyAgLyogQ2hyb21lIDEwLTI1LCBTYWZhcmkgNS4xLTYgKi9cbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gcmlnaHQsICM0RDUzNTgsICM0QjUxNTYpOyAvKiBXM0MsIElFIDEwKy8gRWRnZSwgRmlyZWZveCAxNissIENocm9tZSAyNissIE9wZXJhIDEyKywgU2FmYXJpIDcrICovXG4gICAgZm9udC1zaXplOiA5MCU7XG59XG4ubXktY3VzdG9tLWNsYXNzIC5hcnJvdzo6YmVmb3JlIHtcbiAgICBib3JkZXItdG9wLWNvbG9yOiAjNEQ1MzU4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/term-detail/term-detail.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/term-detail/term-detail.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n    <div class=\"container\">\n        <app-search></app-search>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-2 col-md-1 col-lg-2\"></div>\n        <div class=\"col-sm-8 col-md-10 col-lg-8\">\n            <h5 id=\"term-value\" class=\"text-center\"> Terme : {{ term }} </h5>\n            <div *ngIf=\"nbDefinitions > 0\" id=\"definitions\" class=\"container-fluid\">\n                <br>\n                <ul class=\"list-group list-definitions\">\n                    <button class=\"list-group-item d-flex align-items-center btn-cool-blues btn-rounded\" type=\"button\" (click)=\"isListDefinitionsCollapsed = !isListDefinitionsCollapsed\">\n                        <div class=\"mr-auto p-2\">\n                            Définitions\n                        </div>\n                        <span class=\"badge badge-primary badge-pill shadow\">{{ nbDefinitions }}</span>\n                        <span class=\"badge\"><i class=\"fas\" [ngClass]=\"{'fa-angle-down': isListDefinitionsCollapsed, 'fa-angle-up': !isListDefinitionsCollapsed}\"></i></span>\n                    </button>\n                    <div [ngbCollapse]=\"isListDefinitionsCollapsed\" class=\"collapse\">\n                        <li *ngFor=\"let def of definitions; index as i;\"\n                        class=\"list-group-item\">\n                            <small><b>{{ i + 1 }}.</b> {{ def }}</small>\n                        </li>\n                    </div>\n                </ul>\n            </div>\n            <div *ngIf=\"nbRefinements > 0\" id=\"semantic-refinements\" class=\"container-fluid\">\n                <br>\n                <ul class=\"list-group list-refinements\">\n                    <button class=\"list-group-item d-flex align-items-center btn-cool-blues btn-rounded\" type=\"button\" (click)=\"isListRefinementsCollapsed = !isListRefinementsCollapsed\">\n                        <div class=\"mr-auto p-2\">\n                            Raffinements sémantiques\n                        </div>\n                        <span class=\"badge badge-primary badge-pill shadow\">{{ nbRefinements }}</span>\n                        <span class=\"badge\"><i class=\"fas\" [ngClass]=\"{'fa-angle-down': isListRefinementsCollapsed, 'fa-angle-up': !isListRefinementsCollapsed}\"></i></span>\n                    </button>\n                    <div [ngbCollapse]=\"isListRefinementsCollapsed\" class=\"collapse\">\n                        <li *ngFor=\"let ref of semanticRefinements\"\n                            class=\"list-group-item\">\n                            <small><b>{{ ref.value }}</b></small>\n                        </li>\n                    </div>\n                </ul>\n            </div>\n            <br>\n            <div *ngIf=\"availableRelationships.length > 0\" class=\"container-fluid\">\n                <ul class=\"list-group list-relationships\">\n                    <button class=\"list-group-item d-flex align-items-center btn-cool-blues btn-rounded\" type=\"button\" (click)=\"isListRelationshipsCollapsed = !isListRelationshipsCollapsed\">\n                        <div class=\"mr-auto p-2\">\n                            Types de relations\n                        </div>\n                        <span class=\"badge badge-primary badge-pill shadow\">{{ availableRelationships.length }}</span>\n                        <span class=\"badge\"><i class=\"fas\" [ngClass]=\"{'fa-angle-down': isListRelationshipsCollapsed, 'fa-angle-up': !isListRelationshipsCollapsed}\"></i></span>\n                    </button>\n                    <div [ngbCollapse]=\"isListRelationshipsCollapsed\" class=\"collapse\">\n                        <li class=\"list-group-item\">\n                            <div class=\"btn-group row d-flex justify-content-between\" ngbRadioGroup name=\"radioListRelationships\" data-toggle=\"buttons\" [(ngModel)]=\"first\">\n                                <div *ngFor=\"let key of availableRelationships ; first as isFirst\">\n                                    <label ngbButtonLabel class=\"btn-sm btn-outline-info col\" ngbTooltip=\"{{ relationshipsNames[key].desc }}\" tooltipClass=\"my-custom-class\" (click)=\"showRelation(key, $event)\">\n                                          <input ngbButton type=\"radio\" [value]=\"key\"> {{ relationshipsNames[key].name }}\n                                    </label>\n                                </div>\n                            </div>\n                        </li>\n                    </div>\n                </ul>\n            </div>\n            <br>\n            <div class=\"row\">\n                <div class=\"col\">\n                    <div class=\"row row-cols-2\">\n                        <div *ngIf=\"nbTerms > 0\" class=\"col\">\n                            <div class=\"container\">\n                                <ul class=\"list-group list-outgoingRelationships\">\n                                    <button class=\"list-group-item d-flex align-items-center btn-cool-blues btn-rounded\" type=\"button\" (click)=\"isListOutgoingRelationshipsCollapsed = !isListOutgoingRelationshipsCollapsed\">\n                                        <div class=\"mr-auto p-2\">\n                                            Termes (relations sortantes)\n                                        </div>\n                                        <span class=\"badge badge-primary badge-pill shadow\">{{ nbTerms }}</span>\n                                        <span class=\"badge\"><i class=\"fas\" [ngClass]=\"{'fa-angle-down': isListOutgoingRelationshipsCollapsed, 'fa-angle-up': !isListOutgoingRelationshipsCollapsed}\"></i></span>\n\n                                    </button>\n                                    <div [ngbCollapse]=\"isListOutgoingRelationshipsCollapsed\" class=\"collapse container\">\n                                        <br>\n                                        <div class=\"terms\">\n                                            <div class=\"btn-group btn-block\" ngbRadioGroup name=\"radioTriRelationsSortantes\" data-toggle=\"buttons\">\n                                                <label ngbButtonLabel class=\"btn-outline-primary\">\n                                                    <input ngbButton type=\"radio\" (click)=\"sortByWeight()\" [value]=\"1\"> Tri par poids\n                                                </label>\n                                                <label ngbButtonLabel class=\"btn-outline-primary\">\n                                                    <input ngbButton type=\"radio\" (click)=\"sortAlphabetically()\" [value]=\"2\"> Tri alphabétique\n                                                </label>\n                                            </div>\n                                            <br>\n                                            <br>\n                                            <table class=\"table table-striped table-sm\">\n                                                <thead>\n                                                    <tr>\n                                                        <th scope=\"col-8-sm\">Terme</th>\n                                                        <th scope=\"col-2-sm\">Poids</th>\n                                                    </tr>\n                                                </thead>\n                                                <tbody>\n                                                    <tr *ngFor=\"let term of associatedTerms | slice: (pageOutRels-1)\n                                                    * pageOutRelsSize : (pageOutRels-1) * pageOutRelsSize + pageOutRelsSize\">\n                                                        <td><small>\n                                                            <a [routerLink]=\"['/term-detail',term.value]\" (click)=\"onResearch(term.value)\" style=\"color:black;\">{{ term.value }}</a>&nbsp;&#160;\n                                                            <a *ngIf=\"term.relationId == 777\" target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://fr.wikipedia.org/wiki/{{ term.value }}\" style=\"color:black;\">\n                                                                <i class=\"fab fa-wikipedia-w\"></i>\n                                                            </a>\n                                                        </small></td>\n                                                        <td><small>{{ term.weight }}</small></td>\n                                                    </tr>\n                                                </tbody>\n                                            </table>\n                                            <div class=\"d-flex flex-wrap justify-content-between p-2\">\n                                                <ngb-pagination\n                                                    [(page)]=\"pageOutRels\"\n                                                    [rotate]=\"true\"\n                                                    [maxSize]=\"5\"\n                                                    [boundaryLinks]=\"true\"\n                                                    size=\"sm\"\n                                                    [pageSize]=\"pageOutRelsSize\"\n                                                    [collectionSize]=\"associatedTerms.length\">\n                                                </ngb-pagination>\n                                                <select [(ngModel)]=\"pageOutRelsSize\"\n                                                    style=\"width: auto\"\n                                                    id=\"inputNbTerms\"\n                                                    class=\"custom-select custom-select-sm\">\n                                                    <option [ngValue]=\"25\">25 termes par page</option>\n                                                    <option [ngValue]=\"50\">50 termes par page</option>\n                                                    <option [ngValue]=\"100\">100 termes par page</option>\n                                                    <option [ngValue]=\"200\">200 termes par page</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </ul>\n                            </div>\n                        </div>\n                        <br><br><br>\n                        <div *ngIf=\"nbIncTerms > 0\" class=\"col\">\n                            <div class=\"container\">\n                                <ul class=\"list-group list-incomingRelationships\">\n                                    <button class=\"list-group-item d-flex align-items-center btn-cool-blues\" type=\"button\" (click)=\"isListIncomingRelationshipsCollapsed = !isListIncomingRelationshipsCollapsed\">\n                                        <div class=\"mr-auto p-2\">\n                                            Termes (relations entrantes)\n                                        </div>\n                                        <span class=\"badge badge-primary badge-pill shadow\">{{ nbIncTerms }}</span>\n                                        <span class=\"badge\"><i class=\"fas\" [ngClass]=\"{'fa-angle-down': isListIncomingRelationshipsCollapsed, 'fa-angle-up': !isListIncomingRelationshipsCollapsed}\"></i></span>\n                                    </button>\n                                    <div [ngbCollapse]=\"isListIncomingRelationshipsCollapsed\" class=\"collapse container\">\n                                        <br>\n                                        <div class=\"terms\">\n                                            <div class=\"btn-group btn-block\" ngbRadioGroup name=\"radioTriRelationsEntrantes\" data-toggle=\"buttons\">\n                                                <label ngbButtonLabel class=\"btn-outline-primary\">\n                                                  <input ngbButton type=\"radio\" (click)=\"sortByWeightIncomingTerms()\" [value]=\"1\"> Tri par poids\n                                                </label>\n                                                <label ngbButtonLabel class=\"btn-outline-primary\">\n                                                  <input ngbButton type=\"radio\" (click)=\"sortAlphabeticallyIncomingTerms()\" [value]=\"2\"> Tri alphabétique\n                                                </label>\n                                            </div>\n                                            <br>\n                                            <br>\n                                            <table class=\"table table-striped table-sm\">\n                                                <thead>\n                                                    <tr>\n                                                        <th scope=\"col-8-sm\">Terme</th>\n                                                        <th scope=\"col-2-sm\">Poids</th>\n                                                    </tr>\n                                                </thead>\n                                                <tbody>\n                                                    <tr *ngFor=\"let term of incomingTerms | slice: (pageIncRels-1)\n                                                    * pageIncRelsSize : (pageIncRels-1) * pageIncRelsSize + pageIncRelsSize\">\n                                                        <td><small>\n                                                            <a [routerLink]=\"['/term-detail',term.value]\" (click)=\"onResearch(term.value)\" style=\"color:black;\">{{ term.value }}</a>&nbsp;&#160;\n                                                            <a *ngIf=\"term.relationId == 777\" target=\"_blank\" rel=\"noopener noreferrer\" href=\"https://fr.wikipedia.org/wiki/{{ term.value }}\" style=\"color:black;\">\n                                                                <i class=\"fab fa-wikipedia-w\"></i>\n                                                            </a>\n                                                        </small></td>\n                                                        <td><small>{{ term.weight }}</small></td>\n                                                    </tr>\n                                                </tbody>\n                                            </table>\n                                            <div class=\"d-flex flex-wrap justify-content-between p-2\">\n                                                <ngb-pagination\n                                                    [(page)]=\"pageIncRels\"\n                                                    [rotate]=\"true\"\n                                                    [maxSize]=\"5\"\n                                                    [boundaryLinks]=\"true\"\n                                                    size=\"sm\"\n                                                    [pageSize]=\"pageIncRelsSize\"\n                                                    [collectionSize]=\"incomingTerms.length\">\n                                                </ngb-pagination>\n                                                <select [(ngModel)]=\"pageIncRelsSize\"\n                                                    style=\"width: auto\"\n                                                    id=\"inputNbIncTerms\"\n                                                    class=\"d-flex flex-wrap custom-select custom-select-sm\">\n                                                    <option [ngValue]=\"25\">25 termes par page</option>\n                                                    <option [ngValue]=\"50\">50 termes par page</option>\n                                                    <option [ngValue]=\"100\">100 termes par page</option>\n                                                    <option [ngValue]=\"200\">200 termes par page</option>\n                                                </select>\n                                            </div>\n                                        </div>\n                                    </div>\n                                </ul>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/term-detail/term-detail.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/term-detail/term-detail.component.ts ***!
  \*****************************************************************/
/*! exports provided: TermDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermDetailComponent", function() { return TermDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_term_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/term.service */ "./src/app/services/term.service.ts");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../search/search.component */ "./src/app/components/search/search.component.ts");
/* harmony import */ var _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../assets/json/relationshipsNames.json */ "./src/assets/json/relationshipsNames.json");
var _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_5___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../../../assets/json/relationshipsNames.json */ "./src/assets/json/relationshipsNames.json", 1);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");








var TermDetailComponent = /** @class */ (function () {
    function TermDetailComponent(termService, router) {
        var _this = this;
        this.termService = termService;
        this.router = router;
        this.associatedTerms = [];
        this.incomingTerms = [];
        this.definitions = [];
        this.nbRefinements = 0;
        this.pageIncRels = 1;
        this.pageIncRelsSize = 25;
        this.pageOutRels = 1;
        this.pageOutRelsSize = 25;
        this.semanticRefinements = [];
        this.outgoingRelationships = {};
        this.incomingRelationships = {};
        this.termsRelationId = [];
        this.incomingRelationId = [];
        this.availableRelationships = [];
        this.relationshipsNames = _assets_json_relationshipsNames_json__WEBPACK_IMPORTED_MODULE_5__;
        this.navigationSubscription = this.router.events.subscribe(function (e) {
            // If it is a NavigationEnd event re-initalise the component
            if (e instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]) {
                _this.ngOnInit();
            }
        });
    }
    TermDetailComponent.prototype.ngOnInit = function () {
        this.term = this.termService.getTerm();
        this.termDump = this.termService.getTermDump();
        this.outgoingRelationships = this.termService.getTermOutRels();
        this.incomingRelationships = this.termService.getTermInRels();
        if (!this.checkTermValidity()) {
            return;
        }
        this.semanticRefinements = this.termService.getTermData()
            .semanticRefinements;
        this.searchComponent.termControl.setValue(this.term);
        this.searchComponent.reloadCurrentRoute = true;
        this.displayDump();
        console.log('Temps total de la requête : '
            + this.termService.getRequestDuration() + ' ms.');
    };
    TermDetailComponent.prototype.ngOnDestroy = function () {
        // avoid memory leaks here by cleaning up after ourselves. If we
        // don't then we will continue to run our initialiseInvites()
        // method on every navigationEnd event.
        if (this.navigationSubscription) {
            this.navigationSubscription.unsubscribe();
        }
    };
    TermDetailComponent.prototype.checkTermValidity = function () {
        // Si le terme n'est pas stocké retourner à la page d'accueil
        if (this.term == null) {
            this.router.navigate(['/']);
            return false;
        }
        // Si le serveur indique que le mot n'existe pas, le signaler
        if (this.termDump.includes('\'' + this.term + '\' n\'existe pas !')) {
            this.termExists = false;
            return false;
        }
        this.termExists = true;
        return true;
    };
    TermDetailComponent.prototype.displayDump = function () {
        // Récupération des définitions
        var definitions = this.termDump.split('<def>').pop().split('</def>')[0];
        // Suppression des sauts de lignes inutiles
        definitions = definitions.replace(/\\n/g, '');
        // On récupère le nombre de refinements sémantiques
        this.nbRefinements = Object.keys(this.semanticRefinements).length;
        this.displayDefinitions(definitions);
        this.displaySemanticRefinements(this.semanticRefinements);
        this.displayTerms(this.outgoingRelationships);
        this.displayIncomingTerms(this.incomingRelationships);
    };
    TermDetailComponent.prototype.displayDefinitions = function (dump) {
        // Récupérer les définitions une par une
        this.definitions = dump.split(/<br \/>\d\./);
        this.definitions.shift();
        // Supprimer les balises <br />
        this.definitions.forEach(function (def, index, arr) {
            arr[index] = def.replace(/<br \/>/g, '\n');
        });
        this.nbDefinitions = this.definitions.length;
    };
    TermDetailComponent.prototype.displaySemanticRefinements = function (refinements) {
        this.semanticRefinements = [];
        for (var refinement in refinements) {
            if (refinements.hasOwnProperty(refinement)) {
                this.semanticRefinements.push({
                    'value': refinement,
                    'defs': refinements[refinement].replace(/\\n/g, '\n')
                });
            }
        }
    };
    TermDetailComponent.prototype.displayTerms = function (outgoingRelationships) {
        this.availableRelationships = [];
        for (var key in outgoingRelationships) {
            if (outgoingRelationships.hasOwnProperty(key)) {
                this.availableRelationships.push(key);
            }
        }
        this.showRelation(this.availableRelationships[Object.keys(this.availableRelationships)[0]]);
    };
    TermDetailComponent.prototype.displayIncomingTerms = function (incomingRelationships) {
        for (var key in incomingRelationships) {
            if (incomingRelationships.hasOwnProperty(key)) {
                if (!this.availableRelationships.includes(key)) {
                    this.availableRelationships.push(key);
                }
            }
        }
    };
    TermDetailComponent.prototype.showRelation = function (relationId, event) {
        if (event === void 0) { event = null; }
        if (this.outgoingRelationships.hasOwnProperty(relationId)) {
            this.termsRelationId = this.outgoingRelationships[relationId];
        }
        else {
            this.termsRelationId = [];
        }
        if (this.incomingRelationships.hasOwnProperty(relationId)) {
            this.incomingRelationId = this.incomingRelationships[relationId];
        }
        else {
            this.incomingRelationId = [];
        }
        this.sortByWeight();
        this.sortByWeightIncomingTerms();
        this.associatedTerms = [];
        this.incomingTerms = [];
        this.nbTerms = this.termsRelationId.length;
        this.nbIncTerms = this.incomingRelationId.length;
        for (var _i = 0, _a = this.termsRelationId; _i < _a.length; _i++) {
            var termData = _a[_i];
            var value = termData.term;
            var weight = termData.weight;
            var relationId_1 = termData.relationId;
            this.associatedTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId_1 });
        }
        for (var _b = 0, _c = this.incomingRelationId; _b < _c.length; _b++) {
            var termData = _c[_b];
            var value = termData.term;
            var weight = termData.weight;
            var relationId_2 = termData.relationId;
            this.incomingTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId_2 });
        }
    };
    /* *
     * Tri des termes par ordre alphabétique
     */
    TermDetailComponent.prototype.sortAlphabetically = function () {
        this.associatedTerms = [];
        var collator = new Intl.Collator('fr', {
            numeric: true,
            sensitivity: 'base'
        });
        this.termsRelationId.sort(function (a, b) { return collator.compare(a.term, b.term); });
        for (var _i = 0, _a = this.termsRelationId; _i < _a.length; _i++) {
            var termData = _a[_i];
            var value = termData.term;
            var weight = termData.weight;
            var relationId = termData.relationId;
            this.associatedTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId });
        }
    };
    /* *
     * Tri des termes par poids de relation
     */
    TermDetailComponent.prototype.sortByWeight = function () {
        this.associatedTerms = [];
        this.nbTerms = this.termsRelationId.length;
        // Tri des relations par poids
        this.termsRelationId.sort(function (a, b) { return b.weight - a.weight; });
        for (var _i = 0, _a = this.termsRelationId; _i < _a.length; _i++) {
            var termData = _a[_i];
            var value = termData.term;
            var weight = termData.weight;
            var relationId = termData.relationId;
            this.associatedTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId });
        }
    };
    /* *
     * Tri des relations sortantes par ordre alphabétique
     */
    TermDetailComponent.prototype.sortAlphabeticallyIncomingTerms = function () {
        this.incomingTerms = [];
        var collator = new Intl.Collator('fr', {
            numeric: true,
            sensitivity: 'base'
        });
        this.incomingRelationId.sort(function (a, b) { return collator.compare(a.term, b.term); });
        for (var _i = 0, _a = this.incomingRelationId; _i < _a.length; _i++) {
            var termData = _a[_i];
            var value = termData.term;
            var weight = termData.weight;
            var relationId = termData.relationId;
            this.incomingTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId });
        }
    };
    /* *
     * Tri des relations sortantes par poids de relation
     */
    TermDetailComponent.prototype.sortByWeightIncomingTerms = function () {
        this.incomingTerms = [];
        // Tri des relations par poids
        this.incomingRelationId.sort(function (a, b) { return b.weight - a.weight; });
        this.nbIncTerms = this.incomingRelationId.length;
        for (var _i = 0, _a = this.incomingRelationId; _i < _a.length; _i++) {
            var termData = _a[_i];
            var value = termData.term;
            var weight = termData.weight;
            var relationId = termData.relationId;
            this.incomingTerms.push({ 'value': value, 'weight': weight, 'relationId': relationId });
        }
    };
    TermDetailComponent.prototype.onResearch = function (input) {
        var _this = this;
        this.termService.retrieveDump(input)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["first"])())
            .subscribe(function (data) {
            _this.termService.setStartTime(performance.now());
            _this.termService.setTerm(input);
            _this.termService.setTermData(data);
            _this.addToHistory(input); // Add searched term to search history
            _this.router.navigate(['/term-detail', input]);
            window.scroll(0, 0);
        }, function (error) {
            console.error(error);
        });
    };
    TermDetailComponent.prototype.addToHistory = function (input) {
        var _a;
        // Get time of search
        var searchDateTime = new Date();
        var searchDate = searchDateTime.toLocaleDateString();
        var searchTime = searchDateTime.toLocaleTimeString('en-GB', { hour: "numeric", minute: "numeric" });
        var history = JSON.parse(localStorage.getItem("search_history"));
        if (!history) {
            history = {};
        }
        if (history[searchDate]) {
            history[searchDate].unshift({ term: input, time: searchTime });
        }
        else {
            history = Object.assign((_a = {},
                _a[searchDate] = [{ term: input, time: searchTime }],
                _a), history);
        }
        localStorage.setItem("search_history", JSON.stringify(history));
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_search_search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _search_search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"])
    ], TermDetailComponent.prototype, "searchComponent", void 0);
    TermDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-term-detail',
            template: __webpack_require__(/*! ./term-detail.component.html */ "./src/app/components/term-detail/term-detail.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./term-detail.component.css */ "./src/app/components/term-detail/term-detail.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_term_service__WEBPACK_IMPORTED_MODULE_3__["TermService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], TermDetailComponent);
    return TermDetailComponent;
}());



/***/ }),

/***/ "./src/app/config.ts":
/*!***************************!*\
  !*** ./src/app/config.ts ***!
  \***************************/
/*! exports provided: serverUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "serverUrl", function() { return serverUrl; });
var origin = window.location.origin;
if (origin === 'http://localhost:4200') {
    origin = 'http://localhost:8888';
}
var serverUrl = origin + '/api/';


/***/ }),

/***/ "./src/app/services/term.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/term.service.ts ***!
  \******************************************/
/*! exports provided: TermService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermService", function() { return TermService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config */ "./src/app/config.ts");




var TermService = /** @class */ (function () {
    function TermService(http) {
        this.http = http;
    }
    TermService.prototype.retrieveDump = function (term, rel) {
        if (rel === void 0) { rel = ''; }
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
        };
        return this.http.post(_config__WEBPACK_IMPORTED_MODULE_3__["serverUrl"], {
            'term': term,
            'rel': rel
        }, httpOptions);
    };
    TermService.prototype.getTerm = function () {
        return this.term;
    };
    TermService.prototype.setTerm = function (term) {
        this.term = term;
    };
    TermService.prototype.getRel = function () {
        return this.rel;
    };
    TermService.prototype.setRel = function (rel) {
        if (rel) {
            this.rel = rel;
        }
    };
    TermService.prototype.getTermDump = function () {
        return this.data != null ? this.data.dump : null;
    };
    TermService.prototype.getTermOutRels = function () {
        return this.data != null ? this.data.outgoingRelationships : null;
    };
    TermService.prototype.getTermInRels = function () {
        return this.data != null ? this.data.incomingRelationships : null;
    };
    TermService.prototype.getTermData = function () {
        return this.data;
    };
    TermService.prototype.setTermData = function (data) {
        this.data = data;
    };
    TermService.prototype.getRequestDuration = function () {
        return performance.now() - this.startTime;
    };
    TermService.prototype.setStartTime = function (time) {
        this.startTime = time;
    };
    TermService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TermService);
    return TermService;
}());



/***/ }),

/***/ "./src/assets/json/relationshipsNames.json":
/*!*************************************************!*\
  !*** ./src/assets/json/relationshipsNames.json ***!
  \*************************************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 34, 35, 37, 38, 39, 40, 41, 42, 43, 44, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 119, 120, 121, 122, 123, 124, 125, 126, 127, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 333, 666, 777, 997, 998, 999, default */
/***/ (function(module) {

module.exports = {"0":{"name":"Idée associée","desc":"Énumération des termes les plus étroitement associés au terme"},"1":{"name":"Raffinement sémantique","desc":"Raffinement sémantique vers un usage particulier du terme"},"2":{"name":"Raffinement morphologique","desc":"Raffinement morphologique vers un usage particulier du terme."},"3":{"name":"Domaine","desc":"Domaine(s) relatif(s) au terme.\nEx: pour 'corner', on pourra donner les domaines 'football' ou 'sport'."},"4":{"name":"Partie du discours","desc":"Partie du discours (Nom, Verbe, Adjectif, Adverbe, etc.)."},"5":{"name":"Synonyme","desc":"Synonyme(s) ou quasi-synonyme(s) de ce terme."},"6":{"name":"Hyperonyme","desc":"Énumération des hyperonyme(s) du terme.\nEx: 'animal' et 'mammifère' sont des hyperonymes de 'chat'."},"7":{"name":"Antonyme","desc":"Énumération des antonyme(s) du terme.\nEx: 'chaud' est un antonyme de 'froid'."},"8":{"name":"Hyponyme","desc":"Énumération des hyponyme(s) du terme.\nEx: 'mouche', 'abeille', 'guêpe' sont des hyponymes de 'insecte'."},"9":{"name":"Méronyme","desc":"Énumération des méronyme(s) du terme.\nEx: 'voiture' a comme méronymes : 'porte', 'roue', 'moteur', ..."},"10":{"name":"Holonyme","desc":"Énumération des holonyme(s) du terme.\nEx: 'main', on aura 'bras', 'corps', 'personne', etc... L'holonyme est aussi l'ensemble comme 'classe' pour 'élève'."},"11":{"name":"Locution","desc":"Expression(s) ou mot(s) composé(s) en rapport avec ce terme.\nEx: pour 'moulin', ou pourra avoir 'moulin à vent', 'moulin à eau', 'moulin à café'."},"13":{"name":"Agent","desc":"L'agent (qu'on appelle aussi le sujet) est l'entité qui effectue l'action, ou la subit pour des formes passives ou des verbes d'état.\nEx: des agents typiques de 'courir' peuvent être 'sportif', 'enfant',..."},"14":{"name":"Patient","desc":"Le patient (qu'on appelle aussi l'objet) est l'entité qui subit l'action.\nEx: des patients typiques de manger peuvent être 'viande', 'légume', 'pain', ..."},"15":{"name":"Lieu","desc":"Énumération des lieux typiques où peut se trouver le terme/objet en question.\nEx: pour 'carotte' on a 'potager'."},"16":{"name":"Instrument","desc":"L'instrument est l'objet avec lequel on fait l'action.\nEx: des instruments typiques de 'tuer' peuvent être 'arme', 'pistolet', 'poison', ..."},"17":{"name":"Caractéristique","desc":"Énumération des caractéristiques (adjectifs) possibles/typiques.\nEx: 'liquide', 'froide', 'chaude', pour 'eau'."},"20":{"name":"Magnification","desc":"La magnification ou amplification.\nEx: 'forte fièvre' ou 'fièvre de cheval' pour 'fièvre'. Ou encore 'amour fou' pour 'amour', 'peur bleue' pour 'peur'."},"21":{"name":"Antimagnification","desc":"L'inverse de la magnification.\nEx: 'bruine' pour 'pluie'."},"22":{"name":"Famille lexicale","desc":"Des mots de la même famille lexicale.\nEx: pour 'lait' on pourrait mettre 'laitier', 'laitage', 'laiterie', etc."},"23":{"name":"Caractéristique (inverse)","desc":"Les objets (des noms) possédant typiquement/possiblement la caractérisque suivante.\nEx: 'soleil', 'feu', pour 'chaud'."},"24":{"name":"Agent (inverse)","desc":"Que peut faire ce sujet ?\nEx: chat => miauler, griffer, manger, etc."},"25":{"name":"Instrument (inverse)","desc":"On obtient ici, ce qu'on peut faire avec un instrument donné...\nEx: 'scie' pour 'scier'."},"26":{"name":"Patient (inverse)","desc":"Que peut-on faire à cet objet.\nEx: pour 'pomme', on pourrait avoir 'manger', 'croquer', couper', 'éplucher',  etc."},"27":{"name":"Domaine (inverse)","desc":"À un domaine, on associe des termes"},"28":{"name":"Lieu (inverse)","desc":"À partir d'un lieu, il est énumérer ce qui peut typiquement s'y trouver.\nEx: pour 'Paris' on a 'tour Eiffel'."},"30":{"name":"Lieu (action)","desc":"À partir d'une action (un verbe), il est énumérer les lieux typiques possibles où peut être réalisée cette action."},"31":{"name":"Action (lieu)","desc":"À partir d'une action (un verbe), il est énumérer les lieux typiques possibles où peut être réalisée cette action."},"32":{"name":"Sentiment","desc":"Pour un terme donné, énumération de mots liés à des sentiments ou des emotions associés à ce terme.\nEx: la joie, le plaisir, le dégoût, la peur, la haine, l'amour, etc."},"34":{"name":"Manière","desc":"De quelles manières peut être effectuée l'action (le verbe) proposée ? Il s'agira d'un adverbe ou d'un équivalent comme une locution adverbiale.\nEx: 'rapidement', 'sur le pouce', 'goulûment', 'salement' ... pour 'manger'."},"35":{"name":"Sens/Signification","desc":"Quels sens/signification peut-on donner au terme proposé. Il s'agira de termes (des gloses) évoquant chacun des sens possibles.\nEx: 'forces de l'ordre', 'contrat d'assurance', 'police typographique', ... pour 'police'."},"37":{"name":"Rôle télique","desc":"Le rôle télique indique la fonction du nom ou du verbe. C'est le rôle qu'on lui destine communément pour un artéfact, ou bien un rôle qu'on peut attribuer à un objet naturel.\nEx: 'réchauffer', 'éclairer' pour 'soleil'."},"38":{"name":"Rôle agentif","desc":"De quelle(s) manière(s) peut être crée/construit le terme suivant ?\nOn énumère des verbes transitifs (le terme en est un complément d'objet) qui donnent naissance à l'entité désignée par le terme.\nEx: 'construire' pour 'maison', 'rédiger'/'imprimer' pour 'livre' ou 'lettre'."},"39":{"name":"Verbe -> Action","desc":"Du verbe vers l'action.\nEx: 'construire' -> 'construction', 'jardiner' -> 'jardinage'."},"40":{"name":"Action -> Verbe","desc":"De l'action vers le verbe.\nEx: 'construction' -> 'construire', 'jardinage' -> 'jardiner'."},"41":{"name":"Conséquence","desc":"B (terme donné) est une conséquence possible de A. A et B sont des verbes ou des noms. \nEx: 'tomber' -> 'se blesser', 'faim' -> 'voler', 'allumer' -> 'incendie', 'négligence' -> 'accident', etc.."},"42":{"name":"Cause","desc":"B (terme donné) est une cause possible de A. A et B sont des verbes ou des noms.\nEx: 'se blesser' -> 'tomber', 'vol' -> 'pauvreté', 'incendie' -> 'négligence', 'mort' -> 'maladie', etc.."},"43":{"name":"Adjectif -> Verbe","desc":"Pour un adjectif de potentialité/possibilité, son verbe correspondant.\nEx: 'lavable' -> 'laver'."},"44":{"name":"Verbe -> Adjectif","desc":"Pour un verbe, son adjectif de potentialité/possibilité correspondant.\nEx: 'laver' -> 'lavable'."},"49":{"name":"Action -> Temps","desc":"Valeur temporelle (quel moment) associée au terme indiqué.\nEx: 'dormir' -> 'nuit', 'bronzer' -> 'été', 'fatigue' -> 'soir'."},"50":{"name":"Objet -> Matière","desc":"Quel est la ou les matière/substance pouvant composer l'objet qui suit.\nEx: 'bois' pour 'poutre'."},"51":{"name":"Matière -> Objet","desc":"Quel est la ou les choses qui sont composés de la matière/substance qui suit.\nEx: 'bois' -> 'poutre', 'table',etc.."},"52":{"name":"Successeur temporel","desc":"Qu'est ce qui peut suivre temporellement le terme suivant.\nEx: 'Noêl' -> 'jour de l'an', 'guerre' -> 'paix', 'jour' -> 'nuit',  'pluie' -> 'beau temps'."},"53":{"name":"Produit","desc":"Que peut produire le terme ?\nEx: 'abeille' -> 'miel', 'usine' -> 'voiture', 'agriculteur' -> 'blé',  'moteur' -> 'gaz carbonique'."},"54":{"name":"Résultat","desc":"Le terme est le résultat/produit de qui/quoi ?"},"55":{"name":"Opposition","desc":"A quoi le terme suivant s'oppose/combat/empêche ?\nEx: un médicament s'oppose à la maladie."},"56":{"name":"Opposition (inverse)","desc":"Inverse de opposition.\nEx: une bactérie à comme opposition antibiotique."},"57":{"name":"Implication","desc":"Qu'est-ce que le terme implique logiquement ?\nEx: 'ronfler' -> 'dormir', 'courir' -> 'se déplacer', 'câlin' -> 'contact physique'. (attention ce n'est pas la cause ni le but...)"},"58":{"name":"Quantificateur","desc":"Quantificateur(s) typique(s) pour le terme,  indiquant une quantité.\nEx: 'sucre' -> 'grain', 'morceau' - 'sel' -> 'grain', 'pincée' - 'herbe' -> 'brin', 'touffe'."},"59":{"name":"Masculin","desc":"L'équivalent masculin du terme.\nEx: 'lionne' -> 'lion'."},"60":{"name":"Féminin","desc":"L'équivalent féminin du terme.\nEx: lion -> lionne."},"61":{"name":"Équivalent","desc":"Terme(s) strictement équivalent/identique.\nEx: acronymes et sigles (PS -> parti socialiste), apocopes (ciné -> cinéma), entités nommées (Louis XIV -> Le roi soleil), etc. (attention il ne s'agit pas de synonyme)"},"62":{"name":"Manière (inverse)","desc":"Quelles actions (verbes) peut-on effectuer de cette manière ?\nEx: rapidement -> courir, manger, ..."},"63":{"name":"Implication (verbe/action)","desc":"Les verbes ou actions qui sont impliqués dans la création de l'objet.(Il s'agit des étapes nécessaires à la réalisation du terme).\nEx: pour 'construire' un livre, il faut, imprimer, relier, brocher, etc."},"64":{"name":"Instance","desc":"Une instance d'un 'type' est un individu particulier de ce type. Il s'agit d'une entité nommée.\nEx: 'transatlantique' a pour instance possible 'Titanic'."},"65":{"name":"Verbe -> Réalisation","desc":"Pour un verbe, celui qui réalise l'action (par dérivation morphologique).\nEx: chasser -> chasseur, naviguer -> navigateur."},"67":{"name":"Similaire","desc":"Terme similaire/ressemblant à ..\nEx: le congre est similaire à une anguille."},"68":{"name":"Ensemble -> Élément","desc":"Quel est l'élément qui compose l'ensemble qui suit.\nEx: un essaim est composé d'abeilles."},"69":{"name":"Élement -> Ensemble","desc":"Quel est l'ensemble qui est composé de l'élément qui suit.\nEx: plusieurs abeilles composent un essaim."},"70":{"name":"Processus -> Agent","desc":"Quel est l'acteur de ce processus/événement ?\nEx: 'nettoyage' peut avoir comme acteur 'technicien de surface'."},"71":{"name":"Variante","desc":"Variante(s) du terme.\nEx: yaourt, yahourt, ou encore évènement, événement."},"72":{"name":"Strictement substituable","desc":"Termes strictement substituables, pour termes hors du domaine général, et pour la plupart des noms.\nEx: endométriose intra-utérine -> adénomyose."},"73":{"name":"Plus petit que...","desc":"Qu'est-ce qui est physiquement plus gros que... (la comparaison doit être pertinente).\nEx: 'lapin' plus petit que 'chien'."},"74":{"name":"Plus grand que...","desc":"Qu'est-ce qui est physiquement moins gros que... (la comparaison doit être pertinente).\nEx: 'chien' plus grand que 'lapin'."},"75":{"name":"Accompagné de...","desc":"Le terme est souvent accompagné de, se trouve avec...\nEx: Astérix et Obelix, le pain et le fromage, les fraises et la chantilly."},"76":{"name":"Processus -> Patient","desc":"Quel est le patient de ce processus/événement ?\nEx: 'découpe' peut avoir comme patient 'viande'."},"77":{"name":"Participe passé","desc":"Le participe passé (au masculin singulier) du verbe infinitif.\nEx: pour manger -> mangé."},"78":{"name":"Co-hyponyme","desc":"Énumération des termes co-hyponyme du terme.\nEx: 'chat' et 'tigre' sont des co-hyponymes de 'félin'."},"79":{"name":"Participe présent","desc":"Le participe présent (au masculin singulier) du verbe infinitif.\nEx: pour manger -> mangeant."},"80":{"name":"Processus -> Instrument","desc":"Quel est l'instrument/moyen de ce processus/événement ?\nEx: 'découpe' peut avoir comme instrument 'couteau'."},"99":{"name":"Dérivé morphologique","desc":"Des termes dérivés morphologiquement sont énumérés.\nEx: pour 'lait' on pourrait mettre 'laitier', 'laitage', 'laiterie', etc. (mais pas 'lactose')."},"100":{"name":"Auteur","desc":"Quel est l'auteur de l'oeuvre suivante ?\nEx: 'Le rouge et le noir' -> 'Émile Zola'."},"101":{"name":"Personnage","desc":"Quels sont les personnages présents dans l'oeuvre qui suit ?"},"102":{"name":"Nourriture","desc":"De quoi peut se nourir l'animal suivant ?"},"103":{"name":"Acteur","desc":"A comme acteurs (pour un film ou similaire)."},"104":{"name":"Mode de déplacement","desc":"Mode de déplacement.\nEx: 'chat' -> 'marche'."},"105":{"name":"Interprète","desc":"Interprète de personnages (cinéma ou théâtre)."},"106":{"name":"Couleur","desc":"A comme couleur(s).\nEx: chat -> noir."},"107":{"name":"Cible","desc":"A comme cible(s).\nEx: cible de la maladie : myxomatose => lapin, rougeole => enfant."},"108":{"name":"Symptômes","desc":"A comme symptôme(s).\nEx: symptômes de la maladie : myxomatose => yeux rouges, rougeole => boutons."},"109":{"name":"Prédécesseur temporel","desc":"Qu'est ce qui peut précéder temporellement (inverse de successeur temporel) le terme ?"},"110":{"name":"Diagnostique","desc":"Ex: diagnostique pour la maladie : diabète => prise de sang, rougeole => examen clinique."},"111":{"name":"Prédécesseur spatial","desc":"Qu'est ce qui peut précéder spatialement (inverse de successeur spatial) le terme ?"},"112":{"name":"Successeur spatial","desc":"Qu'est ce qui peut suivre spatialement le terme ?\nEx: 'Locomotive à vapeur' -> 'tender', 'wagon' etc.."},"113":{"name":"Relation (sociale/familliale)","desc":"Relation sociale/familliale entre les individus..."},"114":{"name":"Tributaire (physique/spatial)","desc":"Tributaire de physique/spatial)"},"115":{"name":"Sentiment (inverse)","desc":"Pour un sentiment ou émotion donné, énumération des termes associés.\nEx: pour 'joie', on aurait 'cadeau', 'naissance', 'bonne nouvelle', etc.."},"116":{"name":"Lien","desc":"À quoi le terme est relié ?\nEx: un wagon est relié à un autre wagon ou à une locomotive."},"117":{"name":"Fonction","desc":"La fonction de ce terme par rapport à d'autres. Pour les prépositions notamment, 'chez' -> 'relation'"},"119":{"name":"But (nom/verbe)","desc":"But de l'action (nom ou verbe)."},"120":{"name":"Action/Verbe -> But","desc":"Quel sont les actions ou verbes qui ont le terme cible comme but ?"},"121":{"name":"Personne -> Possession","desc":"Que possède le terme ?\nEx: un soldat possède un fusil, une cavalière des bottes, un fusil, etc.."},"122":{"name":"Possession -> Personne","desc":"Par qui ou quoi est possédé le terme ?\nEx: un fusil est possédé par un soldat."},"123":{"name":"Auxiliaire","desc":"Auxiliaire utilisé pour ce verbe."},"124":{"name":"Prédécesseur logique","desc":"Qu'est ce qui peut précéder le terme (inverse de successeur logique) ?\nEx: A précède B."},"125":{"name":"Successeur logique","desc":"Qu'est ce qui peut suivre logiquement le terme ?\nEx: A -> B, C.."},"126":{"name":"Incompatibilité générique","desc":"Relation d'incompatibilité pour les génériques.\nSi A est incompatible B alors X ne peut pas être à la fois A et B ou alors X est polysémique.\nEx: poisson est incompatible avec oiseau."},"127":{"name":"Incompatibilité","desc":"Relation d'incompatibilité (ne doivent pas être présents ensemble).\nEx: alcool incompatible avec antibiotique."},"129":{"name":"Requiert","desc":"Énumération des termes nécessaires au terme donné.\nEx: 'se reposer' -> 'calme', ou 'pain' -> 'farine'."},"130":{"name":"Instance de...","desc":"Une instance est un individu particulier. Il s'agit d'une entité nommée.\nEx: 'Jolly Jumper' est une instance de 'cheval', 'Titanic' en est une de 'transatlantique'."},"131":{"name":"Concerné par...","desc":"A peut être concerné par B.\nEx: une personne a un rendez-vous a une maladie, une idée, une opinion, etc.."},"132":{"name":"Symptômes (inverse)","desc":"Inverse de symptômes de la maladie.\nEx: myxomatose -> yeux rouges, rougeole -> boutons."},"133":{"name":"Unités","desc":"A comme unités pour une propriété, ou une mesure.\nEx: vitesse a pour unités m/s ou km/h, etc.."},"134":{"name":"Favorise","desc":"Qu'est-ce que le terme donné favorise ?\nEx: un catalyseur favorise une réaction chimique."},"135":{"name":"Circonstances","desc":"Les circonstances possibles pour un événements, ou un objet."},"136":{"name":"Oeuvres","desc":"Quelles sont les oeuvres de l'auteur donné ?"},"137":{"name":"Processus -> Agent (inverse)","desc":"Inverse de Agent-> Processus."},"138":{"name":"Processus -> Patient (inverse)","desc":"Inverse de Patient -> Processus."},"139":{"name":"Processus -> Instrument (inverse)","desc":"Inverse d'Instrument -> Processus"},"149":{"name":"Complément d'agent","desc":"Le complément d'agent est celui qui effectue l'action dans les formes passives.\nEx: pour 'être mangé', la souris est l'agent et le chat le complément d'agent."},"150":{"name":"Bénéficiaire","desc":"Le bénéficiaire est l'entité qui tire bénéfice/préjudice de l'action (un complément d'objet indirect introduit par 'à', 'pour', ...).\nEx: dans - La sorcière donne une pomme à Blanche Neige -, la bénéficiaire est Blanche Neige..."},"151":{"name":"Déscend de.. (évolution)","desc":"Déscend de.. (évolution)"},"152":{"name":"Domaine de substitution","desc":"Quels sont le ou les domaines de substitution pour ce terme quand il est utilisé comme domaine ?\nEx: 'muscle' -> 'anatomie du système musculaire'."},"153":{"name":"Propriété (nom)","desc":"Pour le terme donné, les noms de propriétés pertinents (on ne met que des noms et pas des adjectifs).\nEx: pour 'voiture' on a le 'prix', la 'puissance', la 'longueur', le 'poids', etc.."},"154":{"name":"Voix active","desc":"Pour un verbe à la voix passive, sa voix active.\nEx: pour 'être mangé' on aura 'manger'."},"155":{"name":"Utilise","desc":"Le terme donné peut utiliser un objet ou produit.\nEx: pour 'frigo' on aura 'électricité'."},"156":{"name":"Est utilisé","desc":"Est utilisé par.\nEx: pour 'essence' on a 'voiture'."},"157":{"name":"Adjectif -> Propriété","desc":"Pour un adjectif donné, le nom de propriété correspondant.\nEx: pour 'friable' -> 'friabilité'."},"158":{"name":"Propriété -> Adjectif","desc":"Pour un nom de propriété donné, l'adjectif correspondant.\nEx: pour 'friabilité' -> 'friable'."},"159":{"name":"Adjectif -> Adverbe","desc":"Pour un adjectif donné, l'adverbe correspondant.\nEx: pour 'rapide' -> 'rapidement'."},"160":{"name":"Adverbe -> Adjectif","desc":"Pour un adverbe donné, l'adjectif correspondant.\nEx: pour 'rapidement' -> 'rapide'."},"161":{"name":"Homophone","desc":"Énumération des homophones ou quasi-homophones du terme."},"162":{"name":"Confusion potentielle","desc":"Confusion potentielle avec un autre terme.\nEx: acre et âcre, détonner et détoner."},"163":{"name":"Concerne","desc":"Qui concerne quelque chose ou quelqu'un (inverse de Conserné par).\nEx: 'maladie' -> 'personne', ou 'disparition' -> 'emploi'."},"164":{"name":"Adjectif -> Nom","desc":"Le nom associé à l'adjectif.\nEx: 'urinaire' -> 'urine'."},"165":{"name":"Nom -> Adjectif","desc":"L'adjectif associé au nom.\nEx: 'urine' -> 'urinaire'."},"166":{"name":"Opinion de...","desc":"L'opinion de tel groupe ou telle personne. Utilisé comme relation d'annotation."},"333":{"name":"Translation","desc":"Traduction vers une autre langue."},"666":{"name":"TOTAKI","desc":"Équivalent pour TOTAKI de l'association libre."},"777":{"name":"Wikipédia","desc":"Associations issues de wikipedia."},"997":{"name":"Annotation d'exception","desc":"Relation pour indiquer qu'il s'agit d'une exception par rapport à la cible.\nEx: l'autruche ne vole pas, et c'est une exception par rapport à l'oiseau prototypique."},"998":{"name":"Annotation","desc":"Relation pour annoter (de façon générale) des relations."},"999":{"name":"Inhibition","desc":"Relation d'inhibition, le terme inhibe les termes suivants. Ce terme a tendance à exclure le terme associé."}};

/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/chris/projets/jdm-interface/client/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map