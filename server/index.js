const bodyParser = require('body-parser');
const child_process = require('child_process');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const csrf = require('csurf');
const express = require('express');
const helmet = require('helmet');
const mongoose = require('mongoose');
const morgan = require('morgan')
const path = require('path');
const request = require('request');

require('./src/models/userModel');
const routes = require('./config/routes');
const limiter = require('./config/config').limiter;

const app = express();

app.use(compression());
app.use(cors());
app.use(cookieParser());
// app.use(csrf({ cookie: true }));
app.use(helmet());
app.use(limiter);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('combined'))

/* Get most requested words */
const most_requested = child_process.fork('./src/get_most_requested.js');
most_requested.send('start');

console.log("Fetching most requested words ...");

most_requested.on('message', (msg) => {
    console.log(msg);
});

app.use('/api', routes);
app.use(express.static('dist'));

let port = process.env.PORT || 8888;

app.listen(port, () => {
    console.log('\x1b[32m%s\x1b[0m', 'Server running on port ' + port);
});
