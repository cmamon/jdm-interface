const request = require('request');
const sanitizeHtml = require('sanitize-html');
const cache = require('../../config/config').cache;
const JDM_URL = require('../../config/config').JDM_URL;
const termList = require('../../config/utils/term_list');
const relationshipsNames = require('../../config/utils/relationships_names');

exports.getDump = (req, res, next) => {
    // Prevent XSS attacks
    const term = sanitizeHtml(req.body.term);
    const rel = req.body.rel ? req.body.rel : '';

    try {
        if (rel) throw new Error();
        const data = cache.get(term, true);
        res.json(data);
    } catch (err) {
        /* The term is not yet stored in the cache */
        getTermDump(term, rel).then((result) => {
            res.json(result);
            console.log('End of request.\n');
        }, (err) => {
            console.log(err);
            next(err);
        });
    }
};

function getTermDump(term, rel) {
    const url = JDM_URL + term + '&rel=' + rel;

    console.log('Sending request to url  ' + url);

    return new Promise((resolve, reject) => {
        request({ url: url, encoding: 'latin1' }, (err, req, body) => {
            if (err) return reject(err);

            let data = null;
            let dump = JSON.stringify(body);

            if (dump != null) {
                // Get the content between the "CODE" tags
                dump = dump.split('<CODE>').pop().split('</CODE>')[0];

                (async () => {
                    /* Get term's semantic refinements */
                    const semanticRefinements = extractRefinements(term, body)
                    const semanticRefinementsDefs = {};

                    if (semanticRefinements.length > 0) {
                        console.log(
                            '\x1b[36m%s\x1b[0m',
                            'Fetching semantic refinements...'
                        );
                    }

                    for (const refinement of semanticRefinements) {
                        try {
                            let refinementData = await getRefinementData(
                                term, refinement
                            );

                            semanticRefinementsDefs[refinement] = refinementData;
                        } catch (err) {
                            reject(err);
                            console.log(err);
                        }
                    }

                    let outgoingRelationships;
                    let incomingRelationships;

                    if (!dump.includes('\'' + term + '\' n\'existe pas !')) {
                        outgoingRelationships = getOutgoingRels(dump);
                        incomingRelationships = getIncomingRels(dump);
                    }

                    data = {
                        'dump': dump,
                        'outgoingRelationships': outgoingRelationships,
                        'incomingRelationships': incomingRelationships,
                        'semanticRefinements': semanticRefinementsDefs
                    };

                    try {
                        if (term === '') {
                            cache.set(term, data);
                        }
                    } catch (err) {
                        reject(err);
                        console.error(err);
                    }
                    resolve(data);
                })();
            }
        });
    });
}

function extractRefinements(term, dump) {
    // Trouver des chaines de la forme "term>raffinement"
    const regex = new RegExp(
        "e;\\d+;'" + term + ">.+';1;\\d+;'" + term + ">(.+)'", 'g'
    );

    const semanticRefinements = [];

    let match;
    do {
        match = regex.exec(dump);
        if (match && match[1].indexOf(':') === -1) {
            semanticRefinements.push(match[1]);
        }
    } while (match);

    return semanticRefinements;
}

function getRefinementData(term, refinement) {
    const url = JDM_URL + term + '>' + refinement + '&rel=';

    return new Promise((resolve, reject) => {
        request({ url: url, encoding: 'latin1' }, (err, req, body) => {
            if (err) return reject(err);

            let refinementDefinitions = '';
            const dump = JSON.stringify(body);

            if (dump != null) {
                const data = extractRefData(dump);
                data.semanticRefinements = [];
                refinementDefinitions = data.definitions;

                try {
                    cache.set(term + '>' + refinement, data);
                } catch (err) {
                    reject(err);
                    console.error(err);
                }
            }

            resolve(refinementDefinitions);
        });
    });
}

function extractRefData(dump) {
    const defs = dump.split('<def>').pop().split('</def>')[0];
    const outRels = dump
        .split('sortantes : r;rid;node1;node2;type;w').pop()
        .split('// les relations entrantes')[0];

    const inRels = dump
        .split('entrantes : r;rid;node1;node2;type;w').pop()
        .split('// END')[0];

    return { 'definitions': defs, 'outgoingRelationships': outRels, 'incomingRelationships': inRels};
}

function getOutgoingRels(termDump) {
    const splittedTerms = termDump
        .split('(Entries) : e;eid;\'name\';type;w;\'formated name\' ').pop()
        .split('// les types de relations')[0]
        .split('\\n').filter(t => t !== '');

    const outgoingRelationships = termDump.replace(/\\n/g, '<br />')
        .split('sortantes : r;rid;node1;node2;type;w ').pop()
        .split('// les relations entrantes')[0]
        .split('<br />').filter(t => t !== '');

    // Séparation des entrées et suppression des chaînes vides résultantes
    const terms = {};

    for (const entry of splittedTerms) {
        const termId = entry.split(';')[1];
        const term = entry.split(';')[2];

        if (!term.includes('_') && !term.includes('*') && !term.includes(':')) {
            if (term.includes('>')) {
                terms[termId] = entry.split(';')[5];
            } else {
                // Association "termId" => "term"
                terms[termId] = term;
            }
        }
    }

    Object.keys(terms).forEach((termId) => {
        // Enlever les single quotes qui sont rajoutées autour du mot
        terms[termId] = terms[termId].replace(/'/g, '');
    });

    const arrayOutgoingRelationships = [];

    for (const outRel of outgoingRelationships) {
        const typeRelation = outRel.split(';')[4];
        // Check if the relationship type is valid
        if (Object.keys(relationshipsNames).includes(typeRelation)) {
            arrayOutgoingRelationships.push(outRel.split(';'));
        }
    }

    const relationships = {};
    arrayOutgoingRelationships.forEach((relation) => {
        const termId = relation[3];
        const relId = relation[4];
        const weight = relation[5];

        // Associate the term data to the correct outgoing relationship
        if (termId in terms) {
            const value = {
                'term': terms[termId],
                'weight': parseInt(weight),
                'relationId': parseInt(relId)
            };

            if (relId in relationships) {
                relationships[relId].push(value);
            } else {
                relationships[relId] = [value];
            }
        }
    });

    return relationships;
}

function getIncomingRels(termDump) {
    const splittedTerms = termDump
        .split('(Entries) : e;eid;\'name\';type;w;\'formated name\' ').pop()
        .split('// les types de relations')[0]
        .split('\\n').filter(t => t !== '');

    const incomingRelationships = termDump.replace(/\\n/g, '<br />')
        .split('entrantes : r;rid;node1;node2;type;w ').pop()
        .split('// END')[0]
        .split('<br />').filter(t => t !== '');

    // Séparation des entrées et suppression des chaînes vides résultantes
    const terms = {};

    for (const entry of splittedTerms) {
        const termId = entry.split(';')[1];
        const term = entry.split(';')[2];

        if (!term.includes('_') && !term.includes('*') && !term.includes(':')) {
            if (term.includes('>')) {
                terms[termId] = entry.split(';')[5];
            } else {
                // Association "termId" => "term"
                terms[termId] = term;
            }
        }
    }

    Object.keys(terms).forEach((termId) => {
        terms[termId] = terms[termId].replace(/'/g, '')
    });

    const arrayIncomingRelationships = [];

    for (const inRel of incomingRelationships) {
        const typeRelation = inRel.split(';')[4];
        // Check if the relationship type is valid
        if (Object.keys(relationshipsNames).includes(typeRelation)) {
            arrayIncomingRelationships.push(inRel.split(';'));
        }
    }

    const relationships = {};
    arrayIncomingRelationships.forEach((relation) => {
        const termId = relation[2];
        const relId = relation[4];
        const weight = relation[5];

        // Associate the term data to the correct outgoing relationship
        if (termId in terms) {
            const value = {
                'term': terms[termId],
                'weight': parseInt(weight),
                'relationId': parseInt(relId)
            };

            if (relId in relationships) {
                relationships[relId].push(value);
            } else {
                relationships[relId] = [value];
            }
        }
    });

    return relationships;
}

exports.typeahead = (req, res, next) => {
    const input = req.body.input;

    // Read json term_list to return words (suggestions) starting with the input
    const suggestions = Object.keys(termList).filter((key) => {
        return key.indexOf(input) === 0;
    }).reduce((matches, key) => {
        matches[key] = termList[key];
        return matches;
    }, {});

    // Order suggestions by weight
    const suggestionsOrdered = [];
    Object.keys(suggestions).sort((a, b) => {
        suggestions[b] - suggestions[a]
    }).forEach((key) => {
        suggestionsOrdered.push(key);
    });

    // return top 8 results
    res.json(suggestionsOrdered.slice(0, 8));
};
