const request = require('request');
const fs = require('fs');

const json = JSON.parse(
    fs.readFileSync('./config/utils/most_requested.json', 'utf8')
);

const getTermsData = () => {
    return new Promise((resolve, reject) => {
        Object.keys(json).forEach((term) => {
            const development = 'http://localhost:8888/api/';
            const production = 'https://jdm-interface.herokuapp.com/api/';
            const url = (process.env.NODE_ENV ? production : development);

            const data = { 'term': term };

            request.post({ url: url, json: data }, (err, res, body) => {
                if (err) console.log(err);
            });
        });
    });
};

process.on('message', message => {
    getTermsData().then((result) => {
        process.send('Fetching done.');
    }, (err) => {
        console.log(err);
    });
});
