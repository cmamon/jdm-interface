/**
* In this file we retrieve the lastest version of the list of term from the
* JDM API. We convert the file from txt to json.
**/

const request = require('request');
const fs = require('fs');
const express = require('express');
// const performance = require('perf_hooks').performance;

const app = express();

let port = process.env.PORT || 9999;

app.listen(port, () => {
    console.log('\x1b[32m%s\x1b[0m', 'Server running on port ' + port);

    let url = 'http://www.jeuxdemots.org/JDM-LEXICALNET-FR/';

    request({ url: url }, (err, res, body) => {
        if (err) console.log(err);

        // Find file ending with "LEXICALNET-JEUXDEMOTS-ENTRIES.txt" i.e list of entries
        let regex = new RegExp(
            '<a href="(\\d+-LEXICALNET-JEUXDEMOTS-ENTRIES.txt)">', 'g'
        );

        let filename = regex.exec(body)[1];

        if (filename) {
            url = url + filename;
            console.log('Fetching term list form ' + url + '...');

            // Submit a new request to fetch the .txt file
            request({ url: url, encoding: 'latin1' }, (err, res, body) => {
                console.log('Fetched.');
                console.log('Creating file "term_list.json" ...');

                // Convert and format txt file to JSON
                let json = {};
                regex = new RegExp('(\\d+);(.+);', 'g');

                let match;
                do {
                    match = regex.exec(body);
                    if (match) {
                        // Make the term the key
                        json[match[2]] = parseInt(match[1]);
                    }
                } while (match);

                // Sort the terms to improve search speed
                let jsonOrdered = {};
                Object.keys(json).sort().forEach(function(key) {
                    jsonOrdered[key] = json[key];
                });

                jsonOrdered = JSON.stringify(jsonOrdered, null, 4)

                fs.writeFile('../../config/utils/term_list.json', jsonOrdered, (err) => {
                    if (err) throw err;
                    console.log('File created.');
                });
            });
        }
    });
});
